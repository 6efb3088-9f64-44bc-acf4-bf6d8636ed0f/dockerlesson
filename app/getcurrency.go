package main

import (
	"fmt"
	"encoding/json"
	"log"
	"net/http"
	"io"
)

type currency struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Usdrub string `json:"USDRUB"`
		Eurrub string `json:"EURRUB"`
	} `json:"data"`
}

func getrates() string { 
	var client http.Client
	resp, err := client.Get("http://currate.ru/api/?get=rates&pairs=USDRUB,EURRUB&key=b8c3c53ba9f744e70e3e9b51a4baeff1")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

    bodyBytes, err := io.ReadAll(resp.Body)
    if err != nil {
        log.Fatal(err)
    }
    bodyString := string(bodyBytes)
    return bodyString
}

func getcurrency() currency {
	var result = getrates()
		
	var cur currency
	err := json.Unmarshal([]byte(result), &cur)
	if err != nil {
		log.Fatalf("Error occured during unmarshaling. Error: %s", err.Error())
	}
	return cur
}

func main() {
	
	var cur = getcurrency()
	fmt.Println("USD RUB = ", cur.Data.Usdrub)
	fmt.Println("EUR RUB = ", cur.Data.Eurrub)
}