FROM golang:1.15

ENV PROJECT_DIR get-currency

ADD . /$PROJECT_DIR
WORKDIR /$PROJECT_DIR

RUN echo $PWD && ls -la
RUN go build

ENTRYPOINT ["./get-currency"]
